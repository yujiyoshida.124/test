import os, sys 
import time
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import chromedriver_binary

class Scraping:
    def __init__(self):
        pass
    
    def execute(self):
        target = "https://shikiho.jp/stocks/9437"
        #r = requests.get(target)  
        #soup = BeautifulSoup(r.content, "html.parser")      
        
        options = Options()
        options.add_argument(chromedriver_binary.chromedriver_filename)
        options.add_argument('--headless')  

        driver = webdriver.Chrome(options=options)
        driver.get(target)
        time.sleep(5)
        html = driver.page_source.encode('utf-8')
        soup = BeautifulSoup(html, "html.parser")
        driver.quit()

        #print(soup)
        h1 = "#main > div > div.section > div:nth-child(1) > div.main > div > div.performance > div.matrix > table > thead"
        print(soup.select_one(h1))
        print(soup.title)

    def getGoogleResult(self):
        driver = webdriver.Chrome(chromedriver_binary.chromedriver_filename)
        driver.get('https://www.google.com/')
        time.sleep(5)
        search_box = driver.find_element_by_name("q")
        search_box.send_keys('猫 画像')
        search_box.submit()
        html = driver.page_source.encode('utf-8')
        soup = BeautifulSoup(html, "html.parser")
        print(soup)
        time.sleep(5)
        driver.quit()   

        #h1 = "#main > div > div.section > div:nth-child(1) > div.main > div > div.performance > div.matrix > table > thead"
        #print(soup.select_one("h1"))

if __name__ == '__main__':
    print('start')
    Scraping().execute()
    print('end')